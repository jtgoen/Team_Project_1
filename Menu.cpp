//
//  Menu.cpp
//  RDBMS
//
//  Created by Yash Chitneni on 2/22/14.
//  Copyright (c) 2014 yash_chitneni. All rights reserved.
//

#include "Menu.h"
#include "Token_Generator.h"

using namespace Team_Project_1_Database;

bool Menu::player_exists(Database& soccer_DB, std::string team_name, int jersey_num){
    std::vector<std::string> table_data;
    soccer_DB.execute(Token_Generator::player_exists(team_name, jersey_num), table_data);
    return table_data.size();
}

bool Menu::team_exists(Database& soccer_DB, std::string team_name){
    std::vector<std::string> table_data;
    soccer_DB.execute(Token_Generator::team_exists(team_name), table_data);
    return table_data.size();
}

bool Menu::team_exists(Database& soccer_DB, std::string team_name, std::string league_name){
    std::vector<std::string> table_data;
    soccer_DB.execute(Token_Generator::team_exists(team_name, league_name), table_data);
    return table_data.size();
}

bool Menu::league_exists(Database& soccer_DB, std::string league_name){
    std::vector<std::string> table_data;
    soccer_DB.execute(Token_Generator::league_exists(league_name), table_data);
    return table_data.size();
}

void Menu::create_player_table(Database& soccer_DB){
    soccer_DB.execute(Token_Generator::create_player_table());
}

void Menu::create_team_table(Database& soccer_DB){
    soccer_DB.execute(Token_Generator::create_team_table());
}

void Menu::create_league_table(Database& soccer_DB){
    soccer_DB.execute(Token_Generator::create_league_table());
}

void Menu::display_title() {
    std::cout << "     _________         _      _                _" << std::endl;
    std::cout << "    |  ______ ||      | ||   | ||             | ||" << std::endl;
    std::cout << "    | ||     \\||      | ||   | ||             | ||" << std::endl;
    std::cout << "    | ||__          __| ||_  | ||__     ___   | ||" << std::endl;
    std::cout << "    |  ___| _ / _  |__   __||| / _ \\\\  / _ \\\\ | ||" << std::endl;
    std::cout << "    | ||   | ||| ||   | ||   |  ||| ||| ||| ||| ||" << std::endl;
    std::cout << "    | ||   | ||| ||   | ||   |  ||| ||| ||| ||| ||" << std::endl;
    std::cout << "    /_\\\\    \\___//    |_||    \\____//  \\___// |_||" << std::endl;
    std::cout << "         _____________                      __" << std::endl;
    std::cout << "        /_____  _____/                     / /" << std::endl;
    std::cout << "             / /                          / /_--" << std::endl;
    std::cout << "            / /  _____   ___     _____   / /'_-'  _____   _____" << std::endl;
    std::cout << "           / /  / ___/  / _ |   / ___/  / / |    / ___/  / ___/" << std::endl;
    std::cout << "          / /  / /     / /| |  / /__   / /| |   / /__/  / /" << std::endl;
    std::cout << "         /_/  /_/     /_/ |_| /____/  /_/ |_|  /____/  /_/"        << std::endl;
    
    
    //blbalbualubabu
}

void Menu::original_menu(Database& soccer_DB){
    std::cout << "\nMAIN MENU:";
    std::cout << "\nFor the following commands, please type the coinciding number of the command and press enter." << std::endl;
    std::cout << "To return to the main menu at any point, type \"return\"." << std::endl;
    std::cout << "1. Create a League/Teams/Players" << std::endl;
    std::cout << "2. Delete League/Team/Player" << std::endl;
    std::cout << "3. View Leagues/Teams/Players" << std::endl;
    std::cout << "4. View League/Team/Player Stats" << std::endl;
    std::cout << "5. Play Game" << std::endl;
    std::cout << "6. Transfer Player" << std::endl;
    std::cout << "7. View Unique Jersey Numbers" << std::endl;
    std::cout << "8. Exit Program" << std::endl;
    std::cout << " -> ";
}

void Menu::view_submenu(Database& soccer_DB){
    std::cout << "\nView Submenu" << std::endl;
    std::cout << "1. View Leagues" << std::endl;
    std::cout << "2. View Teams" << std::endl;
    std::cout << "3. View Players" << std::endl;
    std::cout << "4. Return" << std::endl;
    std::string input;
    int choice = 0;
    do{
        std::cout << " -> ";
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        choice = atoi(input.c_str());
    }while(choice <= 0 || choice >= 5);
    switch(choice){
    case 1:
        Menu::view_league(soccer_DB);
        break;
    case 2:
        Menu::view_team(soccer_DB);
        break;
    case 3:
        Menu::view_player(soccer_DB);
        break;
    case 4:
        return;
    };
}

void Menu::create_submenu(Database& soccer_DB){
    std::cout << "\nCreate Submenu" << std::endl;
    std::cout << "1. Create Leagues" << std::endl;
    std::cout << "2. Create Teams" << std::endl;
    std::cout << "3. Create Players" << std::endl;
    std::cout << "4. Return" << std::endl;
    std::string input;
    int choice = 0;
    do{
        std::cout << " -> ";
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        choice = atoi(input.c_str());
    }while(choice <= 0 || choice >= 5);
    switch(choice){
    case 1: 
        Menu::create_league_menu(soccer_DB);
        break;
    case 2: 
        Menu::create_team_menu(soccer_DB);
        break;
    case 3: 
        Menu::create_player_menu(soccer_DB);
        break;
    case 4:
        return;
    };
}

void Menu::delete_submenu(Database& soccer_DB){
    std::cout << "\nDelete Submenu" << std::endl;
    std::cout << "1. Delete Leagues" << std::endl;
    std::cout << "2. Delete Teams" << std::endl;
    std::cout << "3. Delete Players" << std::endl;
    std::cout << "4. Return" << std::endl;
    std::string input;
    int choice = 0;
    do{
        std::cout << " -> ";
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        choice = atoi(input.c_str());
    }while(choice <= 0 || choice >= 5);
    switch(choice){
    case 1: 
        Menu::delete_league(soccer_DB);
        break;
    case 2: 
        Menu::delete_team(soccer_DB);
        break;
    case 3: 
        Menu::delete_player(soccer_DB);
        break;
    case 4:
        return;
    };
}

void Menu::stats_submenu(Database& soccer_DB){
    std::cout << "\nStats Submenu" << std::endl;
    std::cout << "1. View League Stats" << std::endl;
    std::cout << "2. View Team Stats" << std::endl;
    std::cout << "3. View Player Stats" << std::endl;
    std::cout << "4. Return" << std::endl;
    std::string input;
    int choice = 0;
    do{
        std::cout << " -> ";
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        choice = atoi(input.c_str());
    }while(choice <= 0 || choice >= 5);
    switch(choice){
    case 1: 
        Menu::league_stats_menu(soccer_DB);
        break;
    case 2: 
        Menu::team_stats_menu(soccer_DB);
        break;
    case 3: 
        Menu::player_stats_menu(soccer_DB);
        break;
    case 4:
        return;
    };
}

void Menu::create_league_menu(Database& soccer_DB) {
    std::string league_name;
    std::string country_name;
    std::string sponsor_name;
    
    std::cout << "Choose a League name\n -> ";
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
    std::cout << "What country is the league located in\n -> ";
    std::getline(std::cin, country_name);
    if(country_name == "return" || country_name == "exit" || country_name == "quit" || country_name == "abort") {
        return;
    }
    std::cout << "Who are the official sponsors of the league\n -> ";
    std::getline(std::cin,sponsor_name);
    if(sponsor_name == "return" || sponsor_name == "exit" || sponsor_name == "quit" || sponsor_name == "abort") {
        return;
    }
    
    soccer_DB.execute(Token_Generator::create_league(league_name, country_name, sponsor_name));
    
}

void Menu::delete_league(Database& soccer_DB) {
    std::string league_name;
    
    soccer_DB.execute("SHOW _LEAGUE;");
    std::cout << "Which league do you want to delete\n -> ";
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
    while (!league_exists(soccer_DB, league_name)) {
        std::cout << "Sorry this league does not exist. Please choose a proper league to delete\n -> ";
        std::getline(std::cin, league_name);
				if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
				  return;
				}
    }
    
    soccer_DB.execute(Token_Generator::delete_league(league_name));
}

void Menu::create_team_menu(Database& soccer_DB) {
    std::string team_name;
    std::string league_name;
    std::string city_name;
    std::string sponsor_name;
    int year_founded = 0;
    std::string manager_name;
    std::string kit_color;
    
    std::cout <<"\nEnter the following information about the team:" << std::endl;
    std::cout << "Choose a Team name: ";
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
    std::cout << "What league is the team going to play in: ";
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
        while(!league_exists(soccer_DB, league_name)){
            std::cout << "League does not exist, try again\n -> ";
            std::getline(std::cin, league_name);
            if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
                return;
            }
        }
    std::cout << "What city is the team located in: ";
    std::getline(std::cin, city_name);
    if(city_name == "return" || city_name == "exit" || city_name == "quit" || city_name == "abort") {
        return;
    }
    std::cout << "Who are the official sponsors of the team: ";
    std::getline(std::cin, sponsor_name);
    if(sponsor_name == "return" || sponsor_name == "exit" || sponsor_name == "quit" || sponsor_name == "abort") {
        return;
    }
    do{
        std::cout << "What year was the team founded: ";
        std::string input;
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        year_founded = atoi(input.c_str());
    }while(!year_founded);
    std::cout << "What is the name of the coach: ";
    std::getline(std::cin, manager_name);
    if(manager_name == "return" || manager_name == "exit" || manager_name == "quit" || manager_name == "abort") {
        return;
    }
    std::cout << "What is the color of the kit: ";
    std::getline(std::cin, kit_color);
    if(kit_color == "return" || kit_color == "exit" || kit_color == "quit" || kit_color == "abort") {
        return;
    }
    soccer_DB.execute(Token_Generator::create_team(team_name, league_name, city_name, sponsor_name, year_founded, manager_name, kit_color));
        int current_num_team = num_of_teams(soccer_DB, league_name) + 1;
        std::cout << current_num_team << std::endl;
    soccer_DB.execute(Token_Generator::update_num_teams(league_name, current_num_team));
}

void Menu::delete_team(Database& soccer_DB) {
    std::string team_name;
    
    soccer_DB.execute("SHOW _TEAM;");
    std::cout << "Which team do you want to remove\n -> ";
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
    while (!team_exists(soccer_DB, team_name)) {
        std::cout << "Sorry this team does not exist. Please choose a proper team to delete\n -> ";
        std::getline(std::cin, team_name);
				if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
					return;
				}
    }
    
    soccer_DB.execute(Token_Generator::delete_team(team_name));
}

void Menu::create_player_menu(Database& soccer_DB) {
    std::string player_name;
    std::string team_name;
    int jersey_number;
    std::string position;
    std::string starting = "no";

    std::cout << "Choose a Player name: ";
    std::getline(std::cin, player_name);
    if(player_name == "return" || player_name == "exit" || player_name == "quit" || player_name == "abort") {
        return;
    }
		view_team(soccer_DB);
		std::cout << "What team does he play for: ";
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
		while (!team_exists(soccer_DB, team_name)) {
        std::cout << "Sorry that team does not exist. Please choose to place player in a team that exists\n -> ";
        std::getline(std::cin, team_name);
				if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
					return;
				}
    }
    soccer_DB.execute(Token_Generator::view_team_roster(team_name));
		std::cout << "What position does he play: ";
    std::getline(std::cin, position);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }


    do{
        std::cout << "What will be his jersey number: ";
        std::string input;
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        jersey_number = atoi(input.c_str());
    }while(jersey_number == 0);
    while (player_exists(soccer_DB, team_name, jersey_number)) {
        std::cout << "Sorry that jersey number has already been taken. Please assign a new number\n -> ";
        std::string input;
        std::getline(std::cin, input);
				if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        jersey_number = atoi(input.c_str());
    }
    if(num_of_starters(soccer_DB, team_name) < 11) {
        starting = "yes";
    }
    soccer_DB.execute(Token_Generator::create_player(player_name, jersey_number, team_name, position, starting));
}

void Menu::delete_player(Database& soccer_DB) {
    std::string team_name;
    int player_number;
    
    soccer_DB.execute("SHOW _PLAYER;");
    std::cout << "What team is the player you want to remove on\n -> ";
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
		while (!team_exists(soccer_DB, team_name)) {
        std::cout << "Sorry this team does not exist. Please choose a proper team to delete\n -> ";
        std::getline(std::cin, team_name);
				if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
				  return;
				}
    }
    std::cout << "What is the player's jersey number\n -> ";
    std::string input;
    std::getline(std::cin, input);
    if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
        return;
    }
    player_number = atoi(input.c_str());
    
		while (!player_exists(soccer_DB, team_name, player_number)) {
        std::cout << "Sorry this Player does not exist. Please choose a proper Player to delete\n -> ";
        std::string input;
        std::getline(std::cin, input);
				if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
					return;
				}
        player_number = atoi(input.c_str());
    }

    soccer_DB.execute(Token_Generator::delete_player(player_number, team_name));
}

void Menu::update_player_goals(Database& soccer_DB, std::string team_name, int goals) {
    if (goals > 0) {
        std::vector<std::string> table_data;
    soccer_DB.execute(Token_Generator::view_team_starting_roster(team_name));
    int assists = 0;
    for (int i = 1; i <= goals; i++) {
        int player_number;
        std::cout << "Enter the number of the player that scored the " << i << " goal\n -> ";
        std::string input;
        std::getline(std::cin, input);
				if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        player_number = atoi(input.c_str());
        while(!player_exists(soccer_DB, team_name, player_number)){
            std::cout << "Player does not exist on this team, try again\n -> ";
            std::getline(std::cin, input);
						if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
                return;
            }
            player_number = atoi(input.c_str());
        }
        int current_goals = 0;
        soccer_DB.execute(Token_Generator::get_num_goals(team_name, player_number), table_data);
        if(table_data.size())
            current_goals = atoi(table_data[0].c_str()) + 1;
        soccer_DB.execute(Token_Generator::update_goals_player(team_name, player_number, current_goals));
        int assister_number;
        std::cout << "Enter the number of the player that assisted the " << i << " goal (0 for no assist)\n -> ";
        std::getline(std::cin, input);
				if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        assister_number = atoi(input.c_str());
        while(assister_number && !player_exists(soccer_DB, team_name, assister_number)){
            std::cout << "Player does not exist on this team, try again\n -> ";
            std::getline(std::cin, input);
						if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
                    return;
                }
            assister_number = atoi(input.c_str());
        }
        if(assister_number){
            int current_assists = 0;
            soccer_DB.execute(Token_Generator::get_num_assists(team_name, player_number), table_data);
            if(table_data.size())
                current_assists = atoi(table_data[0].c_str()) + 1;
            soccer_DB.execute(Token_Generator::update_assists_player(team_name, assister_number, current_assists));
            assists++;
        }
    }
    int current_team_assists = 0;
    soccer_DB.execute(Token_Generator::get_num_team_assists(team_name), table_data);
    if(table_data.size())
    current_team_assists = atoi(table_data[0].c_str()) + assists;
    soccer_DB.execute(Token_Generator::update_assists_team(team_name, current_team_assists));
    }
}

void Menu::update_player_cards(Database& soccer_DB, std::string team_name, int cards) {
    if (cards > 0) {
        std::vector<std::string> table_data;
        std::cout << std::endl;
        soccer_DB.execute(Token_Generator::view_team_starting_roster(team_name));
        for (int i = 1; i <= cards; i++) {
            int player_number;
            std::cout << "Enter the number of the player that received the " << i << " card\n -> ";
            std::string input;
            std::getline(std::cin, input);
            if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
                return;
            }
            player_number = atoi(input.c_str());
            while(!player_exists(soccer_DB, team_name, player_number)){
                std::cout << "Player does not exist on this team, try again\n -> ";
                std::getline(std::cin, input);
                if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
                    return;
                }
                player_number = atoi(input.c_str());
            }
            int current_cards = 0;
            soccer_DB.execute(Token_Generator::get_num_cards(team_name, player_number), table_data);
            if(table_data.size()) {
                current_cards = atoi(table_data[0].c_str()) + 1;
            }
            std::cout << current_cards << std::endl;
            soccer_DB.execute(Token_Generator::update_cards_player(team_name, player_number, current_cards));
        }
    }
}

void Menu::play_game_menu(Database& soccer_DB) {
    std::string league_name = "";
    
    std::string first_team_name = "";
    std::string second_team_name = "";
    
        std::string int_input;
    int first_team_goals = 0;
    int second_team_goals = 0;
    
    int first_team_cards = 0;
    int second_team_cards = 0;
    std::vector<std::string> table_data;

    soccer_DB.execute("SHOW _LEAGUE;");
    std::cout << "What league do the teams play in?\n -> ";
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
    while(!league_exists(soccer_DB, league_name)){
        std::cout << "League does not exist, try again\n -> ";
        std::getline(std::cin, league_name);
        if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
            return;
        }
    }
    std::cout << "\n";
    std::cout << "These are the available teams:\n";
    soccer_DB.execute(Token_Generator::view_league_teams(league_name));
    std::cout << "What is the name of the first team?\n -> ";
    std::getline(std::cin, first_team_name);
    if(first_team_name == "return" || first_team_name == "exit" || first_team_name == "quit" || first_team_name == "abort") {
        return;
    }
    while(!team_exists(soccer_DB, first_team_name, league_name)){
        std::cout << "Team does not exist in this league, try again\n -> ";
        std::getline(std::cin, first_team_name);
        if(first_team_name == "return" || first_team_name == "exit" || first_team_name == "quit" || first_team_name == "abort") {
            return;
        }
    }
    std::cout << "\nHere are the possible matchups" << std::endl;
    soccer_DB.execute(Token_Generator::view_league_matchups(league_name, first_team_name));
    do{
				if (first_team_name == second_team_name) {
					   std::cout << first_team_name << " cannot play itself." << std::endl;
        }
        std::cout << "What is the name of the second team?\n -> ";
        std::getline(std::cin, second_team_name);
        if(second_team_name == "return" || second_team_name == "exit" || second_team_name == "quit" || second_team_name == "abort") {
            return;
        }
        while(!team_exists(soccer_DB, second_team_name, league_name)){
            std::cout << "Team does not exist in this league, try again\n -> ";
            std::getline(std::cin, second_team_name);
            if(second_team_name == "return" || second_team_name == "exit" || second_team_name == "quit" || second_team_name == "abort") {
                return;
            }
        }
    }while(first_team_name == second_team_name);
    std::cout << "\nMATCH LINEUP\n";
    soccer_DB.execute(Token_Generator::view_match_lineup(first_team_name, second_team_name));
    std::cout << "How many goals did " << first_team_name << " score\n -> ";
    std::getline(std::cin, int_input);
    if(int_input == "return" || int_input == "exit" || int_input == "quit" || int_input == "abort") {
        return;
    }
    first_team_goals = atoi(int_input.c_str());
    soccer_DB.execute(Token_Generator::get_num_team_goals(first_team_name), table_data);
    int first_team_current_goals = 0;
    if(table_data.size()) {
        first_team_current_goals = first_team_goals + atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_goals_team(first_team_name, first_team_current_goals));
    
    std::cout << "How many goals did " << second_team_name << " score\n -> ";
    std::getline(std::cin, int_input);
    if(int_input == "return" || int_input == "exit" || int_input == "quit" || int_input == "abort") {
        return;
    }
    second_team_goals = atoi(int_input.c_str());
    soccer_DB.execute(Token_Generator::get_num_team_goals(second_team_name), table_data);
    int second_team_current_goals = 0;
    if(table_data.size()) {
        second_team_current_goals = second_team_goals + atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_goals_team(second_team_name, second_team_current_goals));

    match_points(soccer_DB, first_team_name, second_team_name, first_team_goals, second_team_goals);
    
    update_player_goals(soccer_DB, first_team_name, first_team_goals);
    update_player_goals(soccer_DB, second_team_name, second_team_goals);
    
    std::cout << "How many cards did " << first_team_name << " receive: " << std::endl;
    std::getline(std::cin, int_input);
    if(int_input == "return" || int_input == "exit" || int_input == "quit" || int_input == "abort") {
        return;
    }
    first_team_cards = atoi(int_input.c_str());
    soccer_DB.execute(Token_Generator::get_num_team_cards(first_team_name), table_data);
    int first_team_current_cards = 0;
    if(table_data.size()) {
        first_team_current_cards = first_team_cards + atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_cards_team(first_team_name, first_team_current_cards));
    std::cout << "How many cards did " << second_team_name << " receive: " << std::endl;
    std::getline(std::cin, int_input);
    if(int_input == "return" || int_input == "exit" || int_input == "quit" || int_input == "abort") {
        return;
    }
    second_team_cards = atoi(int_input.c_str());
    soccer_DB.execute(Token_Generator::get_num_team_cards(second_team_name), table_data);
    int second_team_current_cards = 0;
    if(table_data.size()) {
        second_team_current_cards = second_team_cards + atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_cards_team(second_team_name, second_team_current_cards));

    update_player_cards(soccer_DB, first_team_name, first_team_cards);
    update_player_cards(soccer_DB, second_team_name, second_team_cards);
}

void Menu::get_unique_numbers(Database& soccer_DB){
    std::string team_1;
    std::string team_2;

    while (true) {
        while (true) {
            std::cout << "Name of team you want unique numbers from: " << std::endl;
            std::getline(std::cin, team_1);
            if(team_1 == "return" || team_1 == "exit" || team_1 == "quit" || team_1 == "abort") {
                return;
            }
            if (team_exists(soccer_DB, team_1)){
                break;
            }
            std::cout << "Team '" << team_1 << "' does not exist. Please enter a name of an existing team." << std::endl;
        }

        soccer_DB.execute(Token_Generator::view_team_roster(team_1));

        while (true) {
            std::cout << "Name of team you want to filter from: " << std::endl;
            std::getline(std::cin, team_2);
            if(team_2 == "return" || team_2 == "exit" || team_2 == "quit" || team_2 == "abort") {
                return;
            }
            if (team_exists(soccer_DB, team_2)){
                break;
            }
            std::cout << "Team '" << team_2 << "' does not exist. Please enter a name of an existing team." << std::endl;
        }
        if (team_1 != team_2){
            break;
        }
        std::cout << "Cannot get unique numbers from same team. Please enter two different teams." << std::endl;
    }

    soccer_DB.execute(Token_Generator::view_team_roster(team_2));
    soccer_DB.execute(Token_Generator::view_unique_jersey_nums(team_1, team_2));
}

void Menu::match_points(Database& soccer_DB, std::string first_team_name, std::string second_team_name, int first_team_goals, int second_team_goals) {
    int first_team_match_points = 0;
    int second_team_match_points = 0;
    std::vector<std::string> table_data;
    if (first_team_goals > second_team_goals) {
        first_team_match_points = 3;
        second_team_match_points = 0;
    }
    else if (first_team_goals == second_team_goals) {
        first_team_match_points = 1;
        second_team_match_points = 1;
    }
    else if (first_team_goals < second_team_goals) {
        first_team_match_points = 0;
        second_team_match_points = 3;
    }
    soccer_DB.execute(Token_Generator::get_num_points(first_team_name), table_data);
    if(table_data.size()) {
        first_team_match_points += atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_points_team(first_team_name, first_team_match_points));
    soccer_DB.execute(Token_Generator::get_num_points(second_team_name), table_data);
    if(table_data.size()) {
        second_team_match_points += atoi(table_data[0].c_str());
    }
    soccer_DB.execute(Token_Generator::update_points_team(second_team_name, second_team_match_points));
}

void Menu::player_stats_menu(Database& soccer_DB) {
    int jersey_num;
    std::string team_name;
    std::cout << "Name of team in which player plays: " << std::endl;
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
    while(!team_exists(soccer_DB, team_name)){
        std::cout << "Team does not exist, try again\n -> ";
        std::getline(std::cin, team_name);
        if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
            return;
        }
    }
    soccer_DB.execute(Token_Generator::view_team_roster(team_name));
    
    std::cout << "Player's jersey number: " << std::endl;
    std::string input;
    std::getline(std::cin, input);
    if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
        return;
    }
    jersey_num = std::stoi(input);
    while(!player_exists(soccer_DB, team_name, jersey_num)){
        std::cout << "Player does not exist on this team, try again\n -> ";
        std::getline(std::cin, input);
        if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
            return;
        }
        jersey_num = std::stoi(input);
    }
    soccer_DB.execute(Token_Generator::view_player_stats(jersey_num, team_name));
}

void Menu::team_stats_menu(Database& soccer_DB) {
    std::string team_name;
    std::string league_name;
    std::cout << "Name of league in which team is: " << std::endl;
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
    while(!league_exists(soccer_DB, league_name)){
        std::cout << "League does not exist, try again\n -> ";
        std::getline(std::cin, league_name);
        if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
            return;
        }
    }
    soccer_DB.execute(Token_Generator::view_league_teams(league_name));
    
    std::cout << "Name of team: " << std::endl;
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
    while(!team_exists(soccer_DB, team_name)){
        std::cout << "Team does not exist, try again\n -> ";
        std::getline(std::cin, team_name);
        if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
            return;
        }
    }
    soccer_DB.execute(Token_Generator::view_team_stats(team_name));
}

void Menu::league_stats_menu(Database& soccer_DB) {
    std::string league_name;
    
    std::cout << "Name of league to view: " << std::endl;
    std::getline(std::cin, league_name);
    if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
        return;
    }
    while(!league_exists(soccer_DB, league_name)){
        std::cout << "League does not exist, try again\n -> ";
        std::getline(std::cin, league_name);
        if(league_name == "return" || league_name == "exit" || league_name == "quit" || league_name == "abort") {
            return;
        }
    }
    soccer_DB.execute(Token_Generator::view_league_stats(league_name));
}

int Menu::num_of_starters(Database& soccer_DB, std::string team_name) {
    std::vector<std::string> starting_eleven;
    soccer_DB.execute(Token_Generator::get_num_starters(team_name), starting_eleven);
    return starting_eleven.size();
}

int Menu::num_of_teams(Database& soccer_DB, std::string league_name){
    std::vector<std::string> team_list;
  soccer_DB.execute(Token_Generator::get_num_teams(league_name), team_list);
  return team_list.size();
}

void Menu::transfer_player(Database& soccer_DB) {
    std::string team_name;
    int player_number;
    std::string new_team_name;
    int new_player_number;
    
    soccer_DB.execute("SHOW _TEAM;");
    std::cout << "What team does the player you're looking to transfer play for\n -> ";
    std::getline(std::cin, team_name);
    if(team_name == "return" || team_name == "exit" || team_name == "quit" || team_name == "abort") {
        return;
    }
    soccer_DB.execute(Token_Generator::view_team_roster(team_name));
    std::cout << "What is the number of the player you want to transfer\n -> ";
    std::string input;
    std::getline(std::cin, input);
    if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
        return;
    }
    player_number = atoi(input.c_str());
		while (!player_exists(soccer_DB, team_name, player_number)) {
        std::cout << player_number << " is not a valid number of a player. Please pick a valid number\n -> ";
        std::string input;
        std::getline(std::cin, input);
				if(input == "return" || input == "exit" || input == "quit" || input == "abort") {
					return;
				}
        player_number = atoi(input.c_str());
    }
    soccer_DB.execute("SHOW _TEAM;");
    std::cout << "What team do you want to transfer player to\n -> ";
    std::getline(std::cin, new_team_name);
    if(new_team_name == "return" || new_team_name == "exit" || new_team_name == "quit" || new_team_name == "abort") {
        return;
    }
		while (!team_exists(soccer_DB, new_team_name)) {
        std::cout << new_team_name << " does not exist. Please pick a proper team.\n -> ";
        std::getline(std::cin, new_team_name);
				if(new_team_name == "return" || new_team_name == "exit" || new_team_name == "quit" || new_team_name == "abort") {
					return;
				}
    }
		while (team_name == new_team_name) {
        std::cout << new_team_name << " is the same as " << team_name << ". Choose a different team to transfer\n -> ";
        std::getline(std::cin, new_team_name);
				if(new_team_name == "return" || new_team_name == "exit" || new_team_name == "quit" || new_team_name == "abort") {
					return;
				}
    }
    soccer_DB.execute(Token_Generator::view_team_roster(new_team_name));
    
    std::cout << "Please choose a number to assign to the player that does not exist\n -> ";
    std::string new_number_input;
    std::getline(std::cin, new_number_input);
    if(new_number_input == "return" || new_number_input == "exit" || new_number_input == "quit" || new_number_input == "abort") {
        return;
    }
    new_player_number = atoi(new_number_input.c_str());
    
    if (player_exists(soccer_DB, new_team_name, new_player_number)) {
        std::cout << "Please choose a number to assign to the player that does not exist\n -> ";
        std::string new_number_input;
        std::getline(std::cin, new_number_input);
        if(new_number_input == "return" || new_number_input == "exit" || new_number_input == "quit" || new_number_input == "abort") {
            return;
        }
        new_player_number = atoi(new_number_input.c_str());
    }
    
    std::string starting = "no";
    
    if (num_of_starters(soccer_DB, new_team_name) < 11) {
        starting = "yes";
    }
    
    soccer_DB.execute(Token_Generator::transfer_player(player_number, new_player_number, team_name, new_team_name, starting));

}

void Menu::view_league(Database& soccer_DB){
    soccer_DB.execute("SHOW _LEAGUE;");
}

void Menu::view_team(Database& soccer_DB){
    soccer_DB.execute("SHOW _TEAM;");
}

void Menu::view_player(Database& soccer_DB){
    soccer_DB.execute("SHOW _PLAYER;");
}
