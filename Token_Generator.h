/*
 class: Tokenizer
 Acts as direct interface with DBMS by generating tokens in a stringstream and 
    dispensing those tokens to the parser.
 */


//standardized main table header titles
//_LEAGUE -> name, country, sponsor, current_champ, num_teams
//_TEAM   -> name, league, city, sponsor, year_founded, manager, kit, points, goals, assists, cards
//_PLAYER -> name, team, jersey_num, position, goals, assists, cards, starter

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace Team_Project_1_Database {

//The "Token_Generator" takes in variable input and generates a single statement of strings
    //that can then be sent to the DBMS engine parser for database creation, commands and operations.
class Token_Generator {
public:
//Main table creation methods
    
    //Creates the main table for storing player data: _PLAYER
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_player_table();
    
    //Creates the main table for storing team data: _TEAM
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_team_table();
    
    //Creates the main table for storing league data: _LEAGUE
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_league_table();

    
//Delete row methods
    
    //Deletes a row corresponding to "league_name" from the _LEAGUE main table
    //Input: the name of the league to delete
    //Output: a tokenized string to send to the DBMS parser
    static std::string delete_league(std::string league_name);
    
    //Deletes a row corresponding to "team_name" from the _TEAM main table
    //Input: the name of the team to delete
    //Output: a tokenized string to send to the DBMS parser
    static std::string delete_team(std::string team_name);
    
    //Deletes a row corresponding to "player_name" from the _PLAYER main table
    //Input: the name of the player to delete
    //Output: a tokenized string to send to the DBMS parser
    static std::string delete_player(int jersey_num, std::string team_name);

    
//Row existence verify methods
    
    //Checks if the player specified exists in the _PLAYER main table
    //Input: the team name of the player, the jersey number of the player
    //Output: a tokenized string to send to the DBMS parser
    static std::string player_exists(std::string team_name, int jersey_num);
    
    //Checks if the team specified exists in the _TEAM main table
    //Input: the name of the team
    //Output: a tokenized string to send to the DBMS parser
    static std::string team_exists(std::string team_name);
    
    //Checks if the team specified exists in the _TEAM main table
    //Input: the name of the team, the league that the team resides in
    //Output: a tokenized string to send to the DBMS parser
    static std::string team_exists(std::string team_name, std::string league_name);
    
    //Checks if the league specified exists in the _LEAGUE main table
    //Input: the name of the league
    //Output: a tokenized string to send to the DBMS parser
    static std::string league_exists(std::string league_name);


//Table row creation methods
    
    //Creates a new row in the _LEAGUE table for the league specified, with distinguishing data
    //Input: the name of the league, the residing country of the league, the league's sponsor
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_league(std::string name, std::string country, std::string sponsor);
    
    ///Creates a new row in the _TEAM table for the team specified, with distinguishing data
    //Input: the name of the team, the team's affiliated league, the residing city of the team,
        //the team's sponsor, the founding year of the team, the team manager, the team's jersey colors ("kit")
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_team(std::string name, std::string league, std::string city, std::string sponsor, int year_founded, std::string manager, std::string kit);
    
    //Creates a new row in the _PLAYER table for the player specified, with distinguishing data
    //Input: the name of the player, the player's jersey number, the team of the player, the player's position
    //Output: a tokenized string to send to the DBMS parser
    static std::string create_player(std::string name, int jersey_num, std::string team, std::string position, std::string starting);
    
    
//Update methods for individual rows in the main tables
    
    //Updates the "goals scored" value for the specified team, in _TEAM
    //Input: the name of the team, the number of goals scored
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_goals_team(std::string team_name, int goals);
    
    //Updates the "goals scored" value for the specified player, in _PLAYER
    //Input: the team name of the player, the player's jersey number, the number of goals scored
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_goals_player(std::string team_name, int jersey_num, int goals);
    
    //Updates the "assists given" value for the specified team in _TEAM
    //Input: the name of the team, the number of assists given
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_assists_team(std::string team_name, int assists);
    
    //Updates the "assists given" value for the specified player in _PLAYER
    //Input: the team name of the player, the player's jersey number, the number of assists given
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_assists_player(std::string team_name, int jersey_num, int assists);
    
    //Updates the "penalty cards received" value for the specified team in _TEAM
    //Input: the name of the team, the number of penalty cards received
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_cards_team(std::string team_name, int cards);
    
    //Updates the "penalty cards received" value for the specified player in _PLAYER
    //Input: the team name of the player, the player's jersey number, the number of cards received
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_cards_player(std::string team_name, int jersey_num, int cards);
    
    //Updates the "league points" value for the specified team in _TEAM
    //Input: the name of the team, the league points achieved
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_points_team(std::string team_name, int points);
    
    //Updates the number of teams in the specified league in _LEAGUE
    //Input: the name of the team, the number of goals scored
    //Output: a tokenized string to send to the DBMS parser
    static std::string update_num_teams(std::string league_name, int num_teams);
    
    
//Accessor methods to get information from the database
    
    //Queries the database for the number of goals scored by the specified player
    //Input: the player's team's name, the player's jersey number
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_goals(std::string team_name, int jersey_num);
    
    //Queries the database for the number of assists given by the specified player
    //Input: the player's team's name, the player's jersey number
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_assists(std::string team_name, int jersey_num);
    
    //Queries the database for the number of cards received by the specified player
    //Input: the player's team's name, the player's jersey number
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_cards(std::string team_name, int jersey_num);
    
    //Queries the database for the number of goals scored by the specified team
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_team_goals(std::string team_name);
    
    //Queries the database for the number of assists given by the specified team
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_team_assists(std::string team_name);
    
    //Queries the database for the number of cards received by the specified team
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_team_cards(std::string team_name);
    
    //Queries the database for the specified team's league points
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_points(std::string team_name);
    
    //Queries the database for the number of starting players in the specified team
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_starters(std::string team_name);
    
    //Queries the database for the number of teams in the specified league
    //Input: the league's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string get_num_teams(std::string league_name);


//Show functions for various rows and tables
    
    //Creates and shows a table consisting of the specified player's row from _PLAYER
    //Input: the player's jersey number, the name of the player's team
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_player_stats(int jersey_num, std::string team_name);
    
    //Creates and shows a table consisting of the specified team's row from _TEAM
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_team_stats(std::string team_name);
    
    //Creates and shows a table consisting of the specified league's row from _LEAGUE
    //Input: the league's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_league_stats(std::string league_name);
    
    //Creates and shows a table of the specified team's roster of players
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_team_roster(std::string team_name);
    
    //Creates and shows a table of the specified team's roster of starting players
    //Input: the team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_team_starting_roster(std::string team_name);
    
    //Creates and shows a table consisting of the starting lineups from
        //the teams specified as team_name1 and team_name2
    //Input: the first team's name, the second team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_match_lineup(std::string team_name1, std::string team_name2);
    
    //Creates and shows a table of the specified league's list of affiliated teams
    //Input: the league's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_league_teams(std::string league_name);
    
    //Creates and shows a table consisting of unique jersey numbers
        //between team_1 and team_2 (set difference)
     //Input: the first team's name, the second team's name
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_unique_jersey_nums(std::string team_1, std::string team_2);
    
    //Creates and shows a table of all the possible
        //team matchups in the specified league (cross product)
    //Input: the league's name, the name of the first team to match up
    //Output: a tokenized string to send to the DBMS parser
    static std::string view_league_matchups(std::string league_name, std::string team_name1);
    
    
//Transfer player method
    
    //Transfers a player to a new team by updating the given player's data to
        //new_player_number, new_team_name and indicating if the player is starting or not
    //Input: the player's current jersey number, the player's new jersey number,
        //the name of the player's current team, the name of the player's new team,
        //"yes" or "no" to indicate if a player is a starter
    //Output: a tokenized string to send to the DBMS parser
    static std::string transfer_player(int player_number, int new_player_number, std::string team_name, std::string new_team_name, std::string starting);
    
};

}   //end Team_Project_1_Database