//
//  Application_Interface.cpp
//  RDBMS
//
//  Created by Yash Chitneni on 2/17/14.
//  Copyright (c) 2014 yash_chitneni. All rights reserved.
//

#include "Token_Generator.h"
#include "Menu.h"

#include <iostream>
#include <vector>
#include <string>

using namespace Team_Project_1_Database;

void open_database();
void close_database();

Database soccer_DB;

int main() {
	open_database();
	atexit(close_database);
	while(true){
		std::string input;
		int choice;
        Menu::display_title();
		Menu::original_menu(soccer_DB);
		std::getline(cin, input);
		choice = atoi(input.c_str());
		switch(choice){
		case 0:
			std::cout << "Error reading input, try again" << std::endl;
			break;
		case 1:
			Menu::create_submenu(soccer_DB);
			break;
		case 2: 
			Menu::delete_submenu(soccer_DB);
			break;
		case 3:
			Menu::view_submenu(soccer_DB);
			break;
		case 4: 
			Menu::stats_submenu(soccer_DB);
			break;
		case 5:
			Menu::play_game_menu(soccer_DB);
			break;
		case 6: 
			Menu::transfer_player(soccer_DB);
            break;
		case 7:
			Menu::get_unique_numbers(soccer_DB);
			break;
		case 8:
			exit(0);
            break;
		}
	}
}

void open_database(){
	std::ifstream exists;
	exists.open("_LEAGUE.db");
	if(exists.fail()){
		Menu::create_league_table(soccer_DB);
		exists.clear();
	}
	else{
		soccer_DB.execute("OPEN _LEAGUE;");
	}
	exists.close();
	exists.open("_TEAM.db");
	if(exists.fail()){
		Menu::create_team_table(soccer_DB);
		exists.clear();
	}
	else{
		soccer_DB.execute("OPEN _TEAM;");
	}
	exists.close();
	exists.open("_PLAYER.db");
	if(exists.fail()){
		Menu::create_player_table(soccer_DB);
		exists.clear();
	}
	else{
		soccer_DB.execute("OPEN _PLAYER;");
	}
	std::cout << "Players, Teams, and Leagues have been loaded.\n";
	return;
}

void close_database(){
	std::ifstream exists;
	exists.open("_LEAGUE.db");
	if(exists.fail()){
		soccer_DB.execute("WRITE _LEAGUE;");
		exists.clear();
	}
	exists.close();
	exists.open("_TEAM.db");
	if(exists.fail()){
		soccer_DB.execute("WRITE _TEAM;");
		exists.clear();
	}
	exists.close();
	exists.open("_PLAYER.db");
	if(exists.fail()){
		soccer_DB.execute("WRITE _PLAYER;");
		exists.clear();
	}
	soccer_DB.execute("CLOSE _LEAGUE;");
	soccer_DB.execute("CLOSE _TEAM;");
	soccer_DB.execute("CLOSE _PLAYER;");
	std::cout << "Players, Teams, and Leagues have been saved.\n";
    std::cout << "Exiting.\n    Thank you for using Futbol Tracker!" << std::endl;
	return;
}


















