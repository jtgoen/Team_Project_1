#include "Token_Generator.h"

std::string Token_Generator::create_player_table(){
	return "CREATE TABLE _PLAYER ( name INTEGER, team INTEGER, jersey_num INTEGER, position INTEGER, goals INTEGER, assists INTEGER, cards INTEGER, starter INTEGER ) PRIMARY KEY ( team , jersey_num );";
}

std::string Token_Generator::create_team_table(){
	return "CREATE TABLE _TEAM ( name INTEGER, league INTEGER, city INTEGER, sponsor INTEGER, year_founded INTEGER, manager INTEGER, kit INTEGER, points INTEGER, goals INTEGER, assists INTEGER, cards INTEGER ) PRIMARY KEY ( name );";
}

std::string Token_Generator::create_league_table(){
	return "CREATE TABLE _LEAGUE ( name INTEGER, country INTEGER, sponsor INTEGER, current_champ INTEGER, num_teams INTEGER ) PRIMARY KEY ( name );";
}

std::string Token_Generator::delete_player(int jersey_num, std::string team_name) {
    std::stringstream token_stream;

    token_stream << "DELETE FROM _PLAYER";
    token_stream << " WHERE team == \"" << team_name << "\" && jersey_num == " << jersey_num << " ;";
    
    return token_stream.str();
}

std::string Token_Generator::delete_league(std::string league_name) {
    std::stringstream token_stream;
    
    token_stream << "DELETE FROM _LEAGUE";
		token_stream << " WHERE league == \"" << league_name << "\" ;";
    
    return token_stream.str();
}

std::string Token_Generator::delete_team(std::string team_name) {
    std::stringstream token_stream;
    
    token_stream << "DELETE FROM _TEAM";
    token_stream << " WHERE team == \"" << team_name << "\" ;";
    
    return token_stream.str();
}

std::string Token_Generator::player_exists(std::string team_name, int jersey_num){
    std::stringstream token_stream;
    token_stream << "player_exists <- select (jersey_num == " << jersey_num;
    token_stream << " && team == \"" << team_name << "\" ) _PLAYER;";
    return token_stream.str();
}

std::string Token_Generator::team_exists(std::string team_name){
    std::stringstream token_stream;
    token_stream << "team_exists <- select (name == \"" << team_name;
    token_stream << "\" ) _TEAM;";
    return token_stream.str();
}

std::string Token_Generator::team_exists(std::string team_name, std::string league_name){
    std::stringstream token_stream;
    token_stream << "team_exists <- select (name == \"" << team_name;
    token_stream << "\" && league == \"" << league_name << "\" ) _TEAM;";
    return token_stream.str();
}

std::string Token_Generator::league_exists(std::string league_name){
    std::stringstream token_stream;
    token_stream << "league_exists <- ( select (name == \"" << league_name;
    token_stream << "\") _LEAGUE ) ;";
    return token_stream.str();
}

std::string Token_Generator::create_league(string name, string country, string sponsor) {
    std::stringstream token_stream;
        
    //league -> NAME, country, sponsor, current_champ, num_teams
    //caps are primary keys
    
    
    token_stream << "INSERT INTO _LEAGUE VALUES FROM (\"" << name;
    token_stream << "\", \"" << country << "\", \"" << sponsor << "\", \"";
    token_stream << "none" << "\", 0);";   /* current champ to null -> " " */
    
    return token_stream.str();
}

std::string Token_Generator::create_team(string name, string league, string city, string sponsor, int year_founded, string manager, string kit) {
    std::stringstream token_stream;
    
    //team -> NAME, city, sponsor, year_founded, MANAGER, kit, points, goals, assists, cards
    
    token_stream << "INSERT INTO _TEAM VALUES FROM (\"" << name << "\", \"" << league;
    token_stream << "\", \"" << city << "\", \"" << sponsor << "\", ";
    token_stream << year_founded << ", \"" << manager << "\", \"" << kit << "\" ,";
    token_stream << " 0, 0, 0, 0);";
    return token_stream.str();
}

std::string Token_Generator::create_player(string name, int jersey_num, string team, string position) {
    //player -> name, TEAM, JERSEY_NUM, position, goals, assists, cards, starter
    
    std::stringstream token_stream;
    
    token_stream << "INSERT INTO _PLAYER VALUES FROM (\"" << name;
    token_stream << "\" , \"" << team << "\", " << jersey_num;
    token_stream << ", \"" << position << "\", 0, 0, 0, \"no\");";
		return token_stream.str();
}

std::string Token_Generator::update_goals_team(string team_name, int goals){
	std::stringstream token_stream;
	token_stream << "UPDATE _TEAM" << " SET goals = " << goals;
	token_stream << " WHERE  name == \"" << team_name << "\" ;";
	return token_stream.str();
}
std::string Token_Generator::update_goals_player(string team_name, int jersey_num, int goals){
	std::stringstream token_stream;
	token_stream << "UPDATE _PLAYER" << " SET goals = " << goals;
	token_stream << " WHERE  team == \"" << team_name << "\" && jersey_num == " << jersey_num << " ;";
	return token_stream.str();
}

std::string Token_Generator::update_assists_player(string team_name, int jersey_num, int assists){
	std::stringstream token_stream;
	token_stream << "UPDATE _PLAYER" << " SET assists = " << assists;
	token_stream << " WHERE  team == \"" << team_name << "\" && jersey_num == " << jersey_num << " ;";

	return token_stream.str();
}

std::string Token_Generator::update_assists_team(string team_name, int assists){
	std::stringstream token_stream;
	token_stream << "UPDATE _TEAM" << " SET assists = " << assists;
	token_stream << " WHERE  name == \"" << team_name << "\" ;";

	return token_stream.str();
}

std::string Token_Generator::update_cards_player(string team_name, int jersey_num, int cards){
	std::stringstream token_stream;
	token_stream << "UPDATE _PLAYER" << " SET cards = " << cards;
	token_stream << " WHERE  team == \"" << team_name << "\" && jersey_num == " << jersey_num << " ;";

	return token_stream.str();
}

std::string Token_Generator::update_cards_team(string team_name, int cards){ 
	std::stringstream token_stream;
	token_stream << "UPDATE _TEAM" << " SET cards = " << cards;
	token_stream << " WHERE  name == \"" << team_name << "\" ;";

	return token_stream.str();
}

std::string Token_Generator::update_points_team(std::string team_name, int points){
	std::stringstream token_stream;
	token_stream << "UPDATE _TEAM SET points = " << points;
	token_stream << " WHERE name == \"" << team_name << "\" ;";
	return token_stream.str();
}

std::string Token_Generator::get_num_goals(string team_name, int jersey_num){
	std::stringstream token_stream;
	token_stream << "num_goals <- project ( goals ) ( select (team == \"" << team_name;
	token_stream << "\" && jersey_num == " << jersey_num << ") _PLAYER);";
	return token_stream.str();
}

std::string Token_Generator::get_num_assists(string team_name, int jersey_num){
	std::stringstream token_stream;
	token_stream << "Num_assists <- project (assists) (select (team == \"" << team_name;
	token_stream << "\" && jersey_num == " << jersey_num << ") _PLAYER);";
	return token_stream.str();
}

std::string Token_Generator::get_num_cards(string team_name, int jersey_num){
	std::stringstream token_stream;
	token_stream << "Num_cards <- project (cards) (select (team == \"" << team_name;
	token_stream << "\" && jersey_num == " << jersey_num << ") _PLAYER);";
	return token_stream.str();

}

std::string Token_Generator::get_num_points(string team_name){
	std::stringstream token_stream;
	token_stream << "Num_points <- project (points) (select (name == \"" << team_name << "\") _TEAM);";
	return token_stream.str();
}

std::string Token_Generator::get_num_team_goals(string team_name){
	std::stringstream token_stream;
	token_stream << "Team_goals <- project (goals) (select (name == \"" << team_name << "\") _TEAM);";
	return token_stream.str();
}

std::string Token_Generator::get_num_starters(std::string team_name) {
    std::stringstream token_stream;
    token_stream << "starter <- project (jersey_num) (select (team == \"" << team_name << "\" && starter == \"yes\") _PLAYER);";
    return token_stream.str();
}

std::string Token_Generator::get_num_teams(std::string league_name){
	std::stringstream token_stream;
  token_stream << "starter <- project (name) (select (league == \"" << league_name << "\") _TEAM);";
  return token_stream.str();
}

std::string Token_Generator::get_num_team_assists(string team_name){
	std::stringstream token_stream;
	token_stream << "Team_assists <- project (assists) (select (name == \"" << team_name << "\") _TEAM);";
	return token_stream.str();
}

std::string Token_Generator::get_num_team_cards(string team_name){
	std::stringstream token_stream;
	token_stream << "Team_cards <- project (cards) (select (name == \"" << team_name << "\") _TEAM);";
	return token_stream.str();
}

std::string Token_Generator::update_num_teams(std::string league_name, int num_teams){
	std::stringstream token_stream;
	token_stream << "UPDATE _LEAGUE" << " SET num_teams = " << num_teams;
	token_stream << " WHERE  name == \"" << league_name << "\" ;";
	return token_stream.str();
}

std::string Token_Generator::view_player_stats(int jersey_num, string team_name) {
    std::stringstream token_stream;
    
    token_stream << "SHOW ( select (jersey_num == " << jersey_num;
    token_stream << " && team == " << team_name << ") _PLAYER);";
    
    return token_stream.str();
}

std::string Token_Generator::view_team_stats(string team_name) {
    
    std::stringstream token_stream;
    
    token_stream << team_name << " <- select (team_name == \"" << team_name;
    token_stream << "\" ) _TEAM;";
    
    return token_stream.str();
}

std::string Token_Generator::view_league_matchups(std::string league_name, std::string team_name1) {
    std::stringstream token_stream;
    
    //SHOW ((project(select(team)))x(rename (project(select(team)))))
    
    
    
    token_stream << "SHOW (" << "(project (name, city, points) ";
    token_stream << "(select (league == \"" << league_name << "\" && name == \"" << team_name1 << "\") _TEAM))";
    token_stream << " * " << "(rename (opposing_name, opposing_city, opposing_points) ";
    token_stream << "(project (name, city, points) (select ";
    token_stream << "(league == \"" << league_name << "\" && name != \"" << team_name1 << "\") _TEAM))));";
    
    //token_stream << "SHOW ( " /*<< "(project (name, city, points) (select (league == \"" << league_name << "\") _TEAM)));"*/ << "(rename (opposing_name, opposing_city, opposing_points) (project (name, city, points) (select (league == \"" << league_name << "\") _TEAM))));";
    
    /*
     CREATE TABLE _TEAM ( name INTEGER, league INTEGER, city INTEGER, sponsor INTEGER, year_founded INTEGER, manager INTEGER, kit INTEGER, points INTEGER, goals INTEGER, assists INTEGER, cards INTEGER ) PRIMARY KEY ( name );
     
     primary key is now only name
     
     CREATE TABLE _LEAGUE ( name INTEGER, country INTEGER, sponsor INTEGER, current_champ INTEGER, num_teams INTEGER ) PRIMARY KEY ( name );
     */
    
    //std::cout << token_stream.str() << std::endl;
    
    return token_stream.str();
}


std::string Token_Generator::view_league_stats(string league_name) {
    
    std::stringstream token_stream;
    
    token_stream << "SHOW ( select (league_name == \"" << league_name;
    token_stream << "\") _LEAGUE ) ;";
    
    return token_stream.str();
}

std::string Token_Generator::view_team_roster(string team_name) {
    
    std::stringstream token_stream;
    
    token_stream << "SHOW (select ( team == \"" << team_name << "\" ) _PLAYER );";
    
    return token_stream.str();
}

std::string Token_Generator::view_team_starting_roster(string team_name) {
    
    std::stringstream token_stream;
    
    token_stream << "SHOW (project (name, jersey_num, position) (select ( team == \"" << team_name << "\" && starter == \"yes\" ) _PLAYER ));";
    return token_stream.str();
}

std::string Token_Generator::view_match_lineup(std::string team_name1, std::string team_name2){
	std::stringstream token_stream;
	token_stream << "SHOW ((project (name, team, jersey_num) ";
	token_stream << "(select (team == \"" << team_name1 << "\" ) _PLAYER))";
	token_stream << " + (project (name, team, jersey_num) ";
	token_stream <<	"(select (team == \"" << team_name2 << "\" ) _PLAYER)));";
	return token_stream.str();
}

std::string Token_Generator::view_unique_jersey_nums(std::string team_1, std::string team_2) {
	std::stringstream token_stream;

	token_stream << "SHOW ((project (jersey_num) ";
	token_stream << "(select ( team == \"" << team_1 << "\" ) _PLAYER))";
	token_stream << "- (project (jersey_num) ";
	token_stream << "(select (team == \"" << team_2 << "\" ) _PLAYER)));";

	return token_stream.str();
}


std::string Token_Generator::view_league_teams(string league_name) {
    
    std::stringstream token_stream;
    
    token_stream << "SHOW (select ( league == \"" << league_name << "\" ) _TEAM );";
    
    return token_stream.str();
}

std::string Token_Generator::transfer_player(int player_number, int new_player_number, std::string team_name, std::string new_team_name, std::string starting) {
    std::stringstream token_stream;
    token_stream << "UPDATE _PLAYER " << "SET jersey_num = " << new_player_number << ", team = \"" << new_team_name;
    token_stream << "\" , starter = \"" << starting << "\" WHERE team == \"" << team_name << "\" && jersey_num == " << player_number << " ;";
    return token_stream.str();
}