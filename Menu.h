//
//  Menu.h
//  RDBMS
//
//  Created by Yash Chitneni on 2/22/14.
//  Copyright (c) 2014 yash_chitneni. All rights reserved.
//

#ifndef __RDBMS__Menu__
#define __RDBMS__Menu__

#include <iostream>
#include "Database.h"


/*
 Main implementation for displaying the menu of the database application.
*/

namespace Team_Project_1_Database{
    class Menu {
    
      //Checks if a player exists in the _PLAYER relation
      //Input: Database to search, team name and jersey number of player
      //Return: Truth value for existence of player
			static bool player_exists(Database& soccer_DB, std::string team_name, int jersey_num);
      
      //Checks if a team exists in the _TEAM relation
      //Input: Database to search, team name
      //Return: Truth value for existence of team
			static bool team_exists(Database& soccer_DB, std::string team_name);
      
      //Checks if a team exists in the _TEAM relation that is in a certain league
      //Input: Database to search, team name
      //Return: Truth value for existence of team that meats criteria
			static bool team_exists(Database& soccer_DB, std::string team_name, std::string league_name);
      
      //Checks if a league exists in the _LEAGUE relation
      //Input: Database to search, league name
      //Return: Truth value for existence of league
			static bool league_exists(Database& soccer_DB, std::string league_name);
      
      //Increment team's points in accordance to the number of goals passed in
      //Input: Database, team names of teams in game, scores for respective team
      //Return: None, but it sends a string to the database that is a command that updates the points in the team relation
			static void match_points(Database& soccer_DB, std::string first_team_name, std::string second_team_name, int first_team_goals, int second_team_goals);

			//Update a player's goals
      //Input: Database, team name and number of goals to add
      //Return: None, but sends a string to the database that is a command that updates the points in the player relation
      static void update_player_goals(Database& soccer_DB, std::string team_name, int goals);
      
      //Update a player's assists
      //Input: Database, team name and number of assists to add
      //Return: None, but sends a string to the database that is a command that updates the assists in the player relation
      static void update_player_assists(Database& soccer_DB, std::string team_name, int assists);
      
      //Update a player's cards
      //Input: Database, team name and number of cards to add
      //Return: None, but sends a string to the database that is a command that updates the cards in the player relation
      static void update_player_cards(Database& soccer_DB, std::string team_name, int cards);
        
      //Get the number of starters that a team has
      //Input: Database, team name
      //Return: Integer that represents number of starters
      static int num_of_starters(Database& soccer_DB, std::string team_name);
      
      //Get the number of starters that a team has
      //Input: Database, team name
      //Return: Integer that represents number of starters
			static int num_of_teams(Database& soccer_DB, std::string league_name);
    public:

      //Send command to database to create a temporary player table
      //Input: Database
      //Return: None, but sends string as command to database to create a temporary player table
			static void create_player_table(Database& soccer_DB);
      
      //Send command to database to create a temporary team table
      //Input: Database
      //Return: None, but sends string as command to database to create a temporary team table
			static void create_team_table(Database& soccer_DB);
      
      //Send command to database to create a temporary league table
      //Input: Database
      //Return: None, but sends string as command to database to create a temporary league table
			static void create_league_table(Database& soccer_DB);

      //Display the title of the application
			static void display_title();
      
      //Display the main menu of the application
      //Input: Database
      static void original_menu(Database& soccer_DB);
      
      //Display the sub-menu for viewing stored tables
      //Input: Database
			static void view_submenu(Database& soccer_DB);
      
      //Display the sub-menu for creating either rows or temporary relations
			static void create_submenu(Database& soccer_DB);
      
      //Display the sub-menu for deleting rows in stored relations
			static void delete_submenu(Database& soccer_DB);
      
      //Display the sub-menu for displaying the stats of rows in stored tables
			static void stats_submenu(Database& soccer_DB);
      
      //Display the menu for adding a league row to the stored _LEAGUE table.
      //Input: Database
      //Return: None
      static void create_league_menu(Database& soccer_DB);
      
      //Display the menu for adding a team row to the stored _TEAM table.
      //Input: Database
      //Return: None
      static void create_team_menu(Database& soccer_DB);
      
      //Display the menu for adding a player row to the stored _PLAYER table.
      //Input: Database
      //Return: None
      static void create_player_menu(Database& soccer_DB);
      
      //Display the menu for removing a league row from the stored _LEAGUE table.
      //Input: Database
      //Return: None, but sends a command to the database as a string to remove the league from the stored table
			static void delete_league(Database& soccer_DB);
      
      //Display the menu for removing a team row from the stored _TEAM table.
      //Input: Database
      //Return: None, but sends a command to the database as a string to remove the team from the stored table
      static void delete_team(Database& soccer_DB);
      
      //Display the menu for removing a player row from the stored _PLAYER table.
      //Input: Database
      //Return: None, but sends a command to the database as a string to remove the player from the stored table
      static void delete_player(Database& soccer_DB);

      //Display the menu for simulating a game played between two teams in the database
      //Input: Database
      //Return: None, but sends command to the database as a string to update the _PLAYER , _TEAM and _LEAGUE tables to reflect the game played
      static void play_game_menu(Database& soccer_DB);
      
      //Display the menu for viewing the unique jersey numbers that a team has when compared to another
      //Input: Database
      //Return: None, but sends command to the database as a string to create a temporary relation that shows the unique numbers of a team
			static void get_unique_numbers(Database& soccer_DB);
        
      //Check if goals were scored by a team during a given game
      //Input: team name and goals scored
      //Return: Truth value to indicate if goals were scored
      static bool goals_scored(std::string team_name, int goals);
      
      //Check if assists were given by a team during a given game
      //Input: team name and assists given
      //Return: Truth value to indicate if assists were given
      static bool assists_given(std::string team_name, int assists);
      
      //Check if cards were taken by a team during a given game
      //Input: team name and cards taken
      //Return: Truth value to indicate if cards were taken
      static bool cards_received(std::string team_name, int cards);
        
      //Display menu for displaying stats of a player in the _PLAYER table
      //Input: Database
      //Return: None
      static void player_stats_menu(Database& soccer_DB);
      
      //Display menu for displaying stats of a team in the _TEAM table
      //Input: Database
      //Return: None
      static void team_stats_menu(Database& soccer_DB);
      
      //Display menu for displaying stats of a league in the _LEAGUE table
      //Input: Database
      //Return: None
      static void league_stats_menu(Database& soccer_DB);
			        
      //Display menu for transferring a player to another team
      //Input: Database
      //Return: None, but sends command to database as a string to update player stats to reflect team transfer and update team stats
      static void transfer_player(Database& soccer_DB);

			//Display menu for viewing a league in the _LEAGUE table
      static void view_league(Database& soccer_DB);
      
      //Display menu for viewing a team in the _TEAM table
			static void view_team(Database& soccer_DB);
      
      //Display menu for viewing a player in the _PLAYER table
			static void view_player(Database& soccer_DB);
    };
}
#endif /* defined(__RDBMS__Menu__) */